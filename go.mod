module framagit.org/fredix/frd

require (
	github.com/BurntSushi/toml v0.3.0
	github.com/kardianos/service v0.0.0-20180823001510-8f267d80f2eb
	github.com/rjeczalik/notify v0.9.1
	golang.org/x/sys v0.0.0-20180909071014-4526dd3c8b56
	gopkg.in/Graylog2/go-gelf.v1 v1.0.0-20170811154226-7ebf4f536d8f
)
