package frdutils

import (
	"framagit.org/fredix/frd/frdlog"
	//"../frdlog"

	// standard packages
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/sys/unix"

	// external packages
	//"github.com/Graylog2/go-gelf/gelf"
	//"github.com/fsnotify/fsnotify"
	//"gopkg.in/Graylog2/go-gelf.v2/gelf"
	"github.com/rjeczalik/notify"
	"gopkg.in/Graylog2/go-gelf.v1/gelf"
)

type Message struct {
	ID          int
	Version     string
	Environment string
	Message     string `json:"short_message"`
	Host        string `json:"host"`
	Level       int    `json:"level"`
	MessageLog  string `json:"_log"`
	File        string `json:"_file"`
	Localtime   string `json:"_localtime"`
}

type Graylog struct {
	Ip       string `toml:"ip"`
	Port     int    `toml:"port"`
	Format   string `toml:"format"`
	Protocol string `toml:"protocol"`
}

type Watcher struct {
	Name                  string
	Environment           string
	Directory             string
	Recursive             bool
	External_rm           string
	External_options      string
	Internal_rm           bool
	Ext_file              string
	File_size             string
	Size_unit             string
	Payload_host          string
	Payload_level         int
	Removetime            string
	Loop_sleep            string
	Removetime_after_open string
	Removefile_after_open bool
}

// wait_files map[path_file]type => wait_files map[/tmp/test/toto.txt]rm (this mean srm delete the file) OR
// wait_files map[/tmp/test/toto.txt]Time.now (the file is open so we should wait before delete :  removetime_after_open = "5m"
var files_counter = struct {
	sync.RWMutex
	wait_files     map[string]string
	open_timestamp map[string]time.Time
}{wait_files: make(map[string]string), open_timestamp: make(map[string]time.Time)}

//var lock_files_counter = sync.RWMutex{}
var g = &sync.WaitGroup{}

var dir_counter = struct {
	sync.RWMutex
	Counter_key int
	wait_dirs   map[int]string
}{wait_dirs: make(map[int]string)}

func LoopDirectory(frdconf *frdlog.FrdConfig, graylog *Graylog, watcher Watcher) {
	dir_counter.wait_dirs[0] = ""
	dir_counter.Counter_key = 0

	go FilesWatcher(frdconf, watcher)

	go func() {
		for {
			ListFilesAndRemove(graylog, frdconf, watcher)
			frdlog.PrintLog(frdconf, "watcher.Loop_sleep : "+watcher.Loop_sleep)
			frdlog.PrintLog(frdconf, "watcher.Ext_file : "+watcher.Ext_file)
			duration, _ := time.ParseDuration(watcher.Loop_sleep)
			frdlog.PrintLog(frdconf, "Duration sleep : "+duration.String())
			g.Wait()
			time.Sleep(duration)
		}
	}()
}

func ListFilesAndRemove(graylog *Graylog, frdconf *frdlog.FrdConfig, watcher Watcher) {

	var ip string = ""
	if len(graylog.Ip) != 0 && graylog.Port != 0 {
		ip = graylog.Ip + ":" + strconv.Itoa(graylog.Port)
	}

	if len(watcher.Ext_file) != 0 {
		files, _ := ioutil.ReadDir(watcher.Directory)
		for _, f := range files {
			file_ext := filepath.Ext(f.Name())
			if file_ext == watcher.Ext_file || watcher.Ext_file == "*" {
				//fmt.Println("ListFilesAndRemove file to remove : ", watcher.Directory+"/"+f.Name())
				payload, err := Payload(frdconf, watcher, watcher.Directory+"/"+f.Name())
				if err == nil {
					g.Add(1)
					go RemoveFile(
						frdconf,
						watcher,
						ip,
						&payload)
				} else {
					frdlog.PrintLog(frdconf, "payload error : "+err.Error())
				}
			}

			time.Sleep(50 * time.Millisecond)

		}
	} else {
		return
	}
}

func Payload(frdconf *frdlog.FrdConfig, watcher Watcher, file string) (gelf.Message, error) {

	// get last modified time
	filename, err := os.Stat(file)
	if err != nil {
		return gelf.Message{}, errors.New("can not stat file : " + err.Error())
	}

	files_counter.RLock()
	if _, exist := files_counter.wait_files[file]; exist {
		type_lock := files_counter.wait_files[file]

		if type_lock == "rm" {
			frdlog.PrintLog(frdconf, "file exist in map wait_files")
			files_counter.RUnlock()
			return gelf.Message{}, errors.New("file exist in map wait_files")
		}

	}
	files_counter.RUnlock()

	filetime := filename.ModTime()
	//fmt.Println("filetime : ", filetime)

	filesize := filename.Size()

	m := gelf.Message{
		Version: "1.1",
		Short:   watcher.Name,
		Full:    filename.Name(),
		Host:    watcher.Payload_host,
		Level:   int32(watcher.Payload_level),
		//MessageLog: messagelog,
		//File:       file,
		//Localtime:  t.Format("01-02-2006T15-04-05"),
		TimeUnix: float64(time.Now().Unix()),
		Facility: "GELF",
		Extra: map[string]interface{}{
			"_file":        file,
			"_size":        filesize,
			"_environment": watcher.Environment,
			"_filetime":    filetime,
			"_application": "frd",
		},
	}

	return m, nil
}

func RemoveFile(frdconf *frdlog.FrdConfig, watcher Watcher, ip string, payload *gelf.Message) {
	defer g.Done()

	file := payload.Extra["_file"].(string)
	//log.Println("check file to remove :", file)
	frdlog.PrintLog(frdconf, "RE boucle ")

	// get last modified time
	filename, err := os.Stat(file)
	if err != nil {
		frdlog.PrintLog(frdconf, "RemoveFile error can not stat file : "+err.Error())
		return
	}

	if filename.IsDir() == false {
		//fmt.Println("not directory : ", filename)
		filesize_bytes := filename.Size()
		//fmt.Println("filesize_bytes : %s", filesize_bytes)

		specifiedsize, err := strconv.Atoi(watcher.File_size[1:len(watcher.File_size)])
		if err != nil {
			frdlog.PrintLog(frdconf, "unable to extract file size watcher.File_size : "+watcher.File_size)
			return
		}

		//fmt.Println("specifiedsize : %s", specifiedsize)

		// convert specifiedsize to bytes
		var filesize_kilobytes int64 = 0
		var filesize_float64 float64 = 0

		switch watcher.Size_unit {
		case "bytes":
			filesize_kilobytes = filesize_bytes

		case "kilobytes":
			filesize_kilobytes = (filesize_bytes / 1024)

		case "megabytes":
			filesize_kilobytes = (filesize_bytes / 1024)
			filesize_megabytes := (float64)(filesize_kilobytes / 1024) // cast to type float64
			filesize_float64 = filesize_megabytes
		case "gigabytes":
			filesize_kilobytes = (filesize_bytes / 1024)
			filesize_megabytes := (float64)(filesize_kilobytes / 1024) // cast to type float64
			filesize_gigabytes := (filesize_megabytes / 1024)
			filesize_float64 = filesize_gigabytes

		case "terabytes":
			filesize_kilobytes = (filesize_bytes / 1024)
			filesize_megabytes := (float64)(filesize_kilobytes / 1024) // cast to type float64
			filesize_gigabytes := (filesize_megabytes / 1024)
			filesize_terabytes := (filesize_gigabytes / 1024)
			filesize_float64 = filesize_terabytes

		default:
			frdlog.PrintLog(frdconf, "size_unit failed : "+watcher.Size_unit)
			return
		}

		switch operator := watcher.File_size[0:1]; operator {
		case "=":
			if filesize_float64 != 0 {
				if int(filesize_float64) != specifiedsize {
					frdlog.PrintLog(frdconf, "filesize not equal : "+frdlog.FloatToString(filesize_float64)+" <> "+strconv.Itoa(specifiedsize))
					//fmt.Println("filesize not equal  : %f, %d", filesize_float64, specifiedsize)
					return
				}
			} else if int(filesize_kilobytes) != specifiedsize {
				frdlog.PrintLog(frdconf, "filesize not equal : "+strconv.Itoa(int(filesize_kilobytes))+" <> "+strconv.Itoa(specifiedsize))
				// fmt.Println("filesize not equal  : %d, %d", filesize_kilobytes, specifiedsize)
				return
			}

		case "<":
			if filesize_float64 != 0 {
				if int(filesize_float64) >= specifiedsize {
					frdlog.PrintLog(frdconf, "filesize : "+frdlog.FloatToString(filesize_float64)+" >= "+strconv.Itoa(specifiedsize))
					//fmt.Println("filesize not <  : %f, %d", filesize_float64, specifiedsize)
					return
				}
			} else if int(filesize_kilobytes) >= specifiedsize {
				frdlog.PrintLog(frdconf, "filesize : "+strconv.Itoa(int(filesize_kilobytes))+" > "+strconv.Itoa(specifiedsize))
				//fmt.Println("filesize not <  : %d, %d", filesize_kilobytes, specifiedsize)
				return
			}

		case ">":
			if filesize_float64 != 0 {
				if int(filesize_float64) <= specifiedsize {
					frdlog.PrintLog(frdconf, "filesize : "+frdlog.FloatToString(filesize_float64)+" < "+strconv.Itoa(specifiedsize))
					//fmt.Println("filesize not >  : %f, %d", filesize_float64, specifiedsize)
					return
				}
			} else if int(filesize_kilobytes) <= specifiedsize {
				frdlog.PrintLog(frdconf, "filesize : "+strconv.Itoa(int(filesize_kilobytes))+" < "+strconv.Itoa(specifiedsize))
				//fmt.Println("filesize not >  : %d, %d", filesize_kilobytes, specifiedsize)
				return
			}

			// fmt.Println("file_size operator : %s, %s, %s", operator, filesize_kilobytes, specifiedsize)
		default:
			frdlog.PrintLog(frdconf, "file_size operator error : "+operator)
			return
		}
	}
	// sized tests ok
	// now check gap files_timestamp

	filetime := filename.ModTime()
	//fmt.Println("filetime : ", filetime)

	tnow := time.Now()

	if _, exist := files_counter.wait_files[file]; exist {
		//fmt.Println("File exist :", ok)
		type_lock := files_counter.wait_files[file]

		if type_lock == "open" && len(watcher.Removetime_after_open) != 0 {

			files_timestamp := files_counter.open_timestamp[file]

			// get the diff
			diff1 := tnow.Sub(files_timestamp)
			//fmt.Println("Lifespan is : ", diff)

			duration1, _ := time.ParseDuration(watcher.Removetime_after_open)
			//fmt.Println("Duration : ", duration)

			if diff1 > duration1 {
				frdlog.PrintLog(frdconf, "> "+watcher.Removetime_after_open+" REMOVE : "+file)

				var err1 = errors.New("")
				if watcher.Internal_rm == true {
					err1 = delete_file(frdconf, file)
				} else {
					err1 = os.Remove(file)
				}
				if err1 != nil {
					frdlog.PrintLog(frdconf, "error on delete file : "+file+" , error : "+err.Error())
					return
				}
				files_counter.Lock()
				delete(files_counter.wait_files, file)
				delete(files_counter.open_timestamp, file)
				files_counter.Unlock()

				return
			}

		}

	}
	// get the diff
	diff2 := tnow.Sub(filetime)
	//fmt.Println("Lifespan is : ", diff)

	duration2, _ := time.ParseDuration(watcher.Removetime)
	//fmt.Println("Duration : ", duration)

	if filename.IsDir() {
		if watcher.Recursive == true {
			frdlog.PrintLog(frdconf, "Remove directory : "+filename.Name())
			if len(watcher.External_rm) == 0 {

				//RemoveFile(frdconf, watcher, ip, payload)

				/*err := os.RemoveAll(file)
				if err != nil {
					frdlog.PrintLog(frdconf, "Remove directory error : "+err.Error())
					return
				}
				*/
				frdlog.PrintLog(frdconf, "DIR : "+filename.Name())
				files, _ := ioutil.ReadDir(file)

				dir_counter.Lock()
				dir_counter.Counter_key += 1
				dir_counter.wait_dirs[dir_counter.Counter_key] = watcher.Directory + "/" + filename.Name() + "/"
				dir_counter.Unlock()
				frdlog.PrintLog(frdconf, "MAP : "+filename.Name())

				frdlog.PrintLog(frdconf, "DELETE DIR ? file : "+file+" files :"+strconv.Itoa(len(files)))

				if len(files) == 0 {
					frdlog.PrintLog(frdconf, "REMOVE DIR : "+file)

					err := unix.Rmdir(file)
					if err != nil {
						frdlog.PrintLog(frdconf, "error on delete directory  : "+file+"/"+" : "+err.Error())
					}
				}

				for _, f := range files {
					file_ext := filepath.Ext(f.Name())
					frdlog.PrintLog(frdconf, "boucle : "+watcher.Directory+"/"+filename.Name()+"/"+f.Name())
					if file_ext == watcher.Ext_file || watcher.Ext_file == "*" {
						frdlog.PrintLog(frdconf, "boucle2 : "+watcher.Directory+"/"+filename.Name()+"/"+f.Name())

						//fmt.Println("ListFilesAndRemove file to remove : ", watcher.Directory+"/"+f.Name())
						//payload, err := Payload(frdconf, watcher, watcher.Directory+"/"+filename.Name()+"/"+f.Name())
						payload, err := Payload(frdconf, watcher, file+"/"+f.Name())

						if err == nil {
							g.Add(1)
							go RemoveFile(
								frdconf,
								watcher,
								ip,
								&payload)
						} else {
							frdlog.PrintLog(frdconf, "payload error : "+err.Error())
						}
					}
					time.Sleep(50 * time.Millisecond)
				}

			} else {
				go secureDelete(frdconf, watcher.External_rm, watcher.External_options, file)
			}
		} else {
			frdlog.PrintLog(frdconf, "config file is set to recursive=false (do not remove directory) : "+filename.Name())
			return
		}

	} else if diff2 > duration2 {
		frdlog.PrintLog(frdconf, "> "+watcher.Removetime+" REMOVE : "+file)

		if len(watcher.External_rm) == 0 {

			var err1 = errors.New("")
			if watcher.Internal_rm == true {
				err1 = delete_file(frdconf, file)
			} else {
				err1 = os.Remove(file)
			}
			if err1 != nil {
				frdlog.PrintLog(frdconf, "error on delete file : "+file+" , error : "+err.Error())
				return
			}

		} else {
			go secureDelete(frdconf, watcher.External_rm, watcher.External_options, file)
		}
	}

	if len(ip) != 0 {
		PushToGraylogUdp(frdconf, ip, payload)
	}

	//	} else {
	//		fmt.Println(filename.Name(), " < ", watcher.Removetime)
	//	}
}

func secureDelete(frdconf *frdlog.FrdConfig, command string, command_options string, file string) {
	files_counter.Lock()
	files_counter.wait_files[file] = "rm"
	files_counter.Unlock()

	cmd := exec.Command(command, command_options, file)
	//cmd := exec.Command(command, command_options)
	outBytes := &bytes.Buffer{}
	errBytes := &bytes.Buffer{}
	cmd.Stdout = outBytes
	cmd.Stderr = errBytes

	frdlog.PrintLog(frdconf, fmt.Sprintf("==> Executing: %s\n", strings.Join(cmd.Args, " ")))

	frdlog.PrintLog(frdconf, "secure delete command : "+command+" "+command_options+" "+file)
	err := cmd.Start()
	if err != nil {
		frdlog.PrintLog(frdconf, "secure delete failed to start : "+err.Error())
		files_counter.Lock()
		delete(files_counter.wait_files, file)
		delete(files_counter.open_timestamp, file)
		files_counter.Unlock()
		return
	}

	err = cmd.Wait()
	if err != nil {
		frdlog.PrintLog(frdconf, "secure delete error : "+err.Error())
		files_counter.Lock()
		delete(files_counter.wait_files, file)
		delete(files_counter.open_timestamp, file)
		files_counter.Unlock()
		return
	} else {
		frdlog.PrintLog(frdconf, "SECURE DELETED : "+file)
	}

	files_counter.Lock()
	delete(files_counter.wait_files, file)
	delete(files_counter.open_timestamp, file)
	files_counter.Unlock()
}

func PushToGraylogUdp(frdconf *frdlog.FrdConfig, ip string, payload *gelf.Message) {
	gelfWriter, err := gelf.NewWriter(ip)
	if err != nil {
		frdlog.PrintLog(frdconf, "gelf.NewWriter error : "+err.Error())
		return
	}
	if err := gelfWriter.WriteMessage(payload); err != nil {
		frdlog.PrintLog(frdconf, "gelf.WriteMessage error: %s"+err.Error())
		return
	}
	frdlog.PrintLog(frdconf, "IP:>"+ip)
	frdlog.PrintLog(frdconf, fmt.Sprintln("payload: ", payload))
}

func PushToGraylogHttp(watcher Watcher, ip string, payload *Message) {

	payload_id := payload.ID
	file := payload.File

	file_name := filepath.Base(file)
	log.Println("file name :", file_name)

	//url := "http://192.168.51.57:12201/gelf"
	fmt.Println("IP:>", ip)
	fmt.Println("PAYLOAD ID:>", payload_id)

	jsonStr, _ := json.Marshal(payload)

	fmt.Println("json: ", string(jsonStr))

	req, err := http.NewRequest("POST", ip, bytes.NewBuffer(jsonStr))
	//    req.Header.Set("X-Custom-Header", "myvalue")
	//    req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
		//log.Fatal("err")
		log.Println("graylog post err  :", err)
		//if watcher_type == "event" {
		//	boltdb.writerChan <- [3]interface{}{"graylog", "", payload}
		//}
		return
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	if strings.Contains(resp.Status, "202") == true {
		log.Println("status: ", resp.Status)

		//t := time.Now()
		//file_files_timestamp := t.Format("01-02-2006T15-04-05") + "-" + strconv.Itoa(t.Nanosecond())

		// compare file_files_timestamp > file.files_timestamp
		//os.Remove(file)

	} else {
		log.Fatal("Graylog server error : ", resp.Status)
		// store payload to boltdb to send it later
		//storeDB(dbfile, &m)

		//if watcher_type == "event" {
		//	boltdb.writerChan <- [3]interface{}{"graylog", "", payload}
		//}
	}
}

func FilesWatcher(frdconf *frdlog.FrdConfig, watcher Watcher) {

	frdlog.PrintLog(frdconf, "FilesWatcher directory : "+watcher.Directory)

	// Make the channel buffered to ensure no event is dropped. Notify will drop
	// an event if the receiver is not able to keep up the sending pace.
	c := make(chan notify.EventInfo, 2)

	// Set up a watchpoint listening for inotify-specific events within a
	// current working directory. Dispatch each InCloseWrite and InMovedTo
	// events separately to c.
	//if err := notify.Watch(watcher.Directory+"/", c, notify.InOpen, notify.InAccess); err != nil {
	//if err := notify.Watch(watcher.Directory+"/export2.txt", c, notify.InOpen, notify.InAccess, notify.InCloseWrite, notify.InModify, notify.InMovedTo); err != nil {
	if err := notify.Watch(watcher.Directory, c, notify.InOpen, notify.InCreate); err != nil {
		log.Fatal(err)
	}
	defer notify.Stop(c)

	frdlog.PrintLog(frdconf, "inotify watch directory : "+watcher.Directory)

	for {
		// Block until an event is received.
		switch ei := <-c; ei.Event() {
		case notify.InCreate:

			// ignore signals events about directory
			if ei.Path() != watcher.Directory {

				frdlog.PrintLog(frdconf, "Created file : "+ei.Path())

				if _, exist := files_counter.wait_files[ei.Path()]; exist {
					//fmt.Println("File exist :", ok)
					type_lock := files_counter.wait_files[ei.Path()]

					frdlog.PrintLog(frdconf, "check if Created file is in map : "+ei.Path())
					if type_lock != "rm" {
						frdlog.PrintLog(frdconf, "remove Created file from map : "+ei.Path())

						files_counter.Lock()
						delete(files_counter.wait_files, ei.Path())
						delete(files_counter.open_timestamp, ei.Path())
						files_counter.Unlock()

					}
				}
			}

		case notify.InOpen:

			// ignore signals events about directory
			if ei.Path() != watcher.Directory {
				if len(watcher.Removetime_after_open) != 0 && (watcher.Removefile_after_open == true) {

					if _, exist := files_counter.wait_files[ei.Path()]; exist {
						frdlog.PrintLog(frdconf, "notify.InOpen, file exist in map")
						break
					}

					filename, err := os.Stat(ei.Path())
					if err != nil {
						frdlog.PrintLog(frdconf, "notify.InOpen error can not stat file : "+err.Error())
						break
					}

					if filename.IsDir() == true {
						frdlog.PrintLog(frdconf, "notify.InOpen, Directory")
						break
					}

					frdlog.PrintLog(frdconf, "Open file : "+ei.Path())

					files_counter.Lock()
					files_counter.wait_files[ei.Path()] = "open"
					t := time.Now()
					files_counter.open_timestamp[ei.Path()] = t
					files_counter.Unlock()

				}
			}

			/*
				case notify.InAccess:
					frdlog.PrintLog(frdconf, "Acces file "+ei.Path())

					files_counter.RLock()
					files_counter.wait_files[path.Base(ei.Path())] = "access"
					files_counter.RUnlock()

				case notify.InCloseWrite:
					frdlog.PrintLog(frdconf, "Editing of "+ei.Path()+"file is done.")

				case notify.InModify:
					frdlog.PrintLog(frdconf, "Modify of "+ei.Path()+"file is done.")

				case notify.InMovedTo:
					frdlog.PrintLog(frdconf, "Moved to "+ei.Path()+"file is done.")

			*/

		}
	}
}

/*
func LogNewWatcher(frdconf *frdlog.FrdConfig, graylog *Graylog, watcher Watcher) {

	frdlog.PrintLog(frdconf, "watched dir: "+watcher.Directory)

	new_watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer new_watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event := <-new_watcher.Events:
				frdlog.PrintLog(frdconf, "event: "+event.String())

				if event.Op&fsnotify.Create == fsnotify.Create {
					frdlog.PrintLog(frdconf, "created file: "+event.Name)
					file_ext := filepath.Ext(event.Name)

					if file_ext == watcher.Ext_file || watcher.Ext_file == "*" {
						data := event.Name
						frdlog.PrintLog(frdconf, "event.Name: "+string(data))

						//var sem = make(chan int, MaxTaches)
						var ip string = ""
						if len(graylog.Ip) != 0 && graylog.Port != 0 {
							ip = graylog.Ip + ":" + strconv.Itoa(graylog.Port)
						}
						payload, err := payload(watcher.Environment, watcher.Name, string(data), event.Name, watcher.Payload_host, watcher.Payload_level)
						if err == nil {
							go RemoveFile(
								frdconf,
								watcher,
								ip,
								&payload)
						}
					}
				}
			case err := <-new_watcher.Errors:
				frdlog.PrintLog(frdconf, "error: "+err.Error())
			}
		}
	}()

	err = new_watcher.Add(watcher.Directory)
	if err != nil {
		log.Fatal(err)
	}
	<-done
}
*/
