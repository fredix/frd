package frdutils

import (
	//"../frdlog"
	"framagit.org/fredix/frd/frdlog"

	"crypto/rand"
	"encoding/base64"
	"errors"
	"math"
	"os"
	"strconv"
)

func delete_file(frdconf *frdlog.FrdConfig, targetFile string) error {

	//var targetFile = "bigfile"

	// make sure we open the file with correct permission
	// otherwise we will get the
	// bad file descriptor error
	file, err := os.OpenFile(targetFile, os.O_RDWR, 0666)

	if err != nil {
		return errors.New("can not open file : " + err.Error())
	}

	defer file.Close()

	// find out how large is the target file

	fileInfo, err := file.Stat()
	if err != nil {
		return errors.New("can not stat file : " + err.Error())
	}

	// calculate the new slice size
	// base on how large our target file is

	var fileSize int64 = fileInfo.Size()
	const fileChunk = 1 * (1 << 20) // 1 MB, change this to your requirement

	// calculate total number of parts the file will be chunked into
	totalPartsNum := uint64(math.Ceil(float64(fileSize) / float64(fileChunk)))

	lastPosition := 0

	for i := uint64(0); i < totalPartsNum; i++ {

		partSize := int(math.Min(fileChunk, float64(fileSize-int64(i*fileChunk))))
		//partZeroBytes := make([]byte, partSize)

		partZeroBytes, _ := GenerateRandomBytes(partSize)

		// fill out the part with zero value
		//copy(partZeroBytes[:], "0")

		// over write every byte in the chunk with 0
		frdlog.PrintLog(frdconf, "Writing to target file from position : "+strconv.Itoa(lastPosition))
		n, err := file.WriteAt([]byte(partZeroBytes), int64(lastPosition))

		if err != nil {
			return errors.New("can not write file : " + err.Error())
		}

		frdlog.PrintLog(frdconf, "Wiped bytes : "+strconv.Itoa(n))

		// update last written position
		lastPosition = lastPosition + partSize
	}
	file.Close()
	// finally remove/delete our file
	err = os.Remove(targetFile)

	if err != nil {
		return errors.New("can not delete file : " + err.Error())
	}
	return nil
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func call_main(frdconf *frdlog.FrdConfig) {
	// Example: this will give us a 44 byte, base64 encoded output
	token, err := GenerateRandomString(32)
	if err != nil {
		// Serve an appropriately vague error to the
		// user, but log the details internally.
	}
	frdlog.PrintLog(frdconf, "token : "+token)

}
